import React, {Component} from 'react'
import classnames from 'classnames'
import {fetchMenu} from 'clients/contentful'

import 'Header/components/MainNav/styles.less'

const buildLinkMenu = (item) => (item.url) ? <a href={item.url} target={item.target}>{item.title}</a> : <span>{item.title}</span>

class MainNav extends Component {
    constructor(props) {
        super(props)
        this.state = {
            menuItems: []
        }
    }

    componentDidMount() {
        fetchMenu().then((menuItems) => this.setState({menuItems})) 
    }
    
    buildMainMenu() {
        return this.state.menuItems.map((item) => {
            const listClass = classnames({'sub-menu': !item.url})
            return (<li className={listClass} key={item.id}>{buildLinkMenu(item)}</li>)
        })
    }

    render() {
        return (
            <nav className='main-menu'>
                <ul>
                    {this.buildMainMenu()}
                </ul>
            </nav>
        )
    }
}

export default MainNav
