import React from 'react'

import 'Footer/components/GeneralInformation/styles.less'

const buildLinks = (links) => links.map(({title, url, target}, index) => (<li key={index}><a href={url} target={target}>{title}</a></li>))

const GeneralInformation = (props) => (
    <section className='general-information'>
        <h4>{props.title}</h4>
        <ul>
            {buildLinks(props.links)}
        </ul>
    </section>
)

export default GeneralInformation
