import React from 'react'
import PointerSVG from 'icons/ic-pin.svg?ex'
import FalabellaBankLogoSVG from 'icons/logo.svg?ex'

import 'Header/components/BrandBanner/styles.less'

const BrandBanner = () => (
    <div className='brand-banner'>
        <div className='office-pointer'>
            <a href='/oficinas'>
                <img alt='Sucursales' src={PointerSVG} />
                &nbsp;Oficinas
            </a>
        </div>
        <div className='home-logo'>
            <a href='/home'>
                <img alt='Banco Falabella' src={FalabellaBankLogoSVG}/>
            </a>
        </div>
        <div className='home-login'>
            <button type='button' className='btn btn-default'>Mi Cuenta</button>
        </div>
    </div>
)

export default BrandBanner
