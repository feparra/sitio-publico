import React, {Component} from 'react'

import 'CreditSimulation/styles.less'

class CreditSimlation extends Component {
    constructor(props) {
        super(props)
        this.state = {
            rut: ''
        }
    }

    onChange = (e) => this.setState({rut: e.target.value})

    render() {
        return (
            <section className='credit-simulation'>
                <h3><span>Simula tu</span> cr&eacute;dito de consumo</h3>
                <form>
                    <input type='text' required placeholder='RUT' onChange={this.onChange} value={this.state.rut} />
                    <input type='submit' className='btn btn-secondary' value='Simular' />
                </form>
            </section>
        )
    }
}

export default CreditSimlation
