import React from 'react'
import UpperNav from 'Header/components/UpperNav'
import BrandBanner from 'Header/components/BrandBanner'
import MainNav from 'Header/components/MainNav'

const Header = () => (
    <header>
        <UpperNav />
        <BrandBanner />
        <MainNav />
    </header>
)

export default Header
