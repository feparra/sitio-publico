import React from 'react'
import 'Header/components/UpperNav/styles.less'

const LINKS = [
    {url: 'https://www.cmr.cl', label: 'CMR'},
    {url: 'https://web.segurosfalabella.com/cl', label: 'Seguros'},
    {url: 'http://www.viajesfalabella.cl/', label: 'Viajes'},
    {url: 'https://www.falabella.com/falabella-cl/', label: 'Falabella'},
    {url: 'http://www.sodimac.cl/sodimac-cl/', label: 'Sodimac'},
    {url: 'http://www.tottus.cl/tottus/', label: 'Tottus'},
    {url: 'http://www.sodimac.cl/sodimac-homy/', label: 'Homy'},
    {url: 'http://www.linio.cl/', label: 'Linio'}
]

const UpperNav = () => {
    const buildLinks = () => LINKS.map((link, index) => (<li key={index}><a href={link.url} target='_blank'>{link.label}</a></li>))

    return (
    <nav className='upper-nav'>
        <ul>
            {buildLinks()}
        </ul>
    </nav>
    )
}

export default UpperNav
