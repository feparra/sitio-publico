const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const {CleanWebpackPlugin} = require('clean-webpack-plugin')

module.exports = {
    mode: 'development',
    entry: {
        app: path.resolve(__dirname, 'src/index.js'),
        imageWorker: path.resolve(__dirname, 'worker/imageWorker.js')
    },
    output: {
        path: path.resolve(__dirname, './dist'),
        filename: '[name].js'
    },
    context: path.resolve(__dirname, 'src/'),
    resolve: {
        modules: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules')
        ],
        alias: {
            icons: path.resolve(__dirname, 'src/assets/icons')
        }
    },
    module: {
        rules: [
            {test: /\.(js|jsx)$/, exclude: /node_modules/, use: ['babel-loader']},
            {test: /\.less$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'less-loader',
                ]
            },
            {test: /\.svg$/, oneOf: [
                { resourceQuery: /in/, use: 'svg-inline-loader'},
                { resourceQuery: /ex/, use: 'file-loader' }
                ]
            },
            {test: /\.(woff|woff2|ttf)$/, use: { loader: 'file-loader', options: { name: 'fonts/[name].[ext]' }}}
        ]
    },
    devServer: {
        compress: true,
        port: 8000,
        filename: 'bundle.js',
        contentBase: [
            path.join(__dirname, 'src/assets')
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({template: path.resolve(__dirname, './src/index.html')})
    ]
}
