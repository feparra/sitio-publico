import React, {Component} from 'react'
import {fetchMainPromotion} from 'clients/contentful'

import 'MainPromotion/styles.less'

class MainPromotion extends Component {
    constructor(props) {
        super(props)
        this.state = {
            promotion: {
                imageUrl: '',
                title: '',
                linkText: '',
                url: '',
                target: ''
            }
        }
    }

    componentDidMount() {
        fetchMainPromotion().then(promotion => this.setState({promotion}))
    }

    render() {
        const style = (this.state.promotion.imageUrl) ? {
            backgroundImage: `url('${this.state.promotion.imageUrl}?fm=jpg&q=30')`
        } : {}
        return(
            <article className='main-promotion'>
                <div className='content-promotion' style={style}>
                    <div className='circle'>
                        <h1>{this.state.promotion.title}</h1>
                        <a className='btn btn-primary' href={this.state.promotion.url} target={this.state.promotion.target}>{this.state.promotion.linkText}</a>
                    </div>
                </div>
            </article>
        )
    }
}

export default MainPromotion
