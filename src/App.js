import React from 'react'
import Header from 'Header'
import MainPromotion from 'MainPromotion'
import CreditSimulation from 'CreditSimulation'
import Banner from 'Banner'
import Footer from 'Footer'

import 'styles.less'

const App = () => {
    return (
        <div>
            <Header />
            <MainPromotion />
            <CreditSimulation />
            <Banner source='homeBanner' />
            <Footer />
        </div>
    )
}

export default App
