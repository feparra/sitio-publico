import React from 'react'

import 'Footer/components/Legality/styles.less'

const Legality = () => (
    <section className='legality'>
        <p>
        Inf&oacute;rmese sobre las entidades autorizadas para emitir Tarjetas de pago en el pa&iacute;s, qui&eacute;nes se encuentran inscritas en los Registros de Emisores de Tarjetas que lleva la SBIF, en <a href='http://www.sbif.cl/' target='_blank'>www.sbif.cl</a>
        </p>
        <p>
        Inf&oacute;rmese sobre la garant&iacute;a estatal de los dep&oacute;sitos en su banco o en <a href='http://www.sbif.cl/' target='_blank'>www.sbif.cl</a>
        </p>
    </section>
)

export default Legality
