import React, {Component} from 'react'
import {fetchFooterLinks} from 'clients/contentful'
import GeneralInformation from 'Footer/components/GeneralInformation'
import SocialNetworks from 'Footer/components/SocialNetworks'
import Legality from 'Footer/components/Legality'

import 'Footer/styles.less'

class Footer extends Component {
    constructor(props) {
        super(props)
        this.state = {
            information: [],
            helpDesk: []
        }
    }

    componentDidMount() {
        fetchFooterLinks().then(({information, helpDesk}) => this.setState({information, helpDesk}))
    }

    render() {
        const businessInformation = {
            links: this.state.information,
            title: 'Nuestro banco'
        }
        const helpDeskInformation = {
            links: this.state.helpDesk,
            title: 'Servicio al cliente'
        }
        return (
            <footer className='footer'>
                <div className='information-container'>
                    <GeneralInformation {...businessInformation} />
                    <GeneralInformation {...helpDeskInformation} />
                    <SocialNetworks />
                </div>
                <div className='legality-container'>
                    <Legality />
                </div>
            </footer>
        )
    }
}

export default Footer
