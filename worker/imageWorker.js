const CACHE_VERSION = 'v2'

self.addEventListener('install', event => {
    console.log('do install')
    self.skipWaiting()
})

self.addEventListener('activate', event => {
    console.log('do activate')
    event.waitUntil(
        caches.keys().then(keys => Promise.all(keys.filter(key => key !== CACHE_VERSION).map(key => caches.delete(key)))).then(self.clients.claim())
    )
})

self.addEventListener('fetch', event => event.respondWith(
    caches.match(event.request).then(response => {
        if(response !== undefined) {
            return response
        }
        else {
            return fetch(event.request).then(response => {
                if(/^https?:\/\/images\.ctfassets\.net/.test(event.request.url)) {
                    const cloneResponse = response.clone()
                    caches.open(CACHE_VERSION).then(cache => cache.put(event.request, cloneResponse))
                }
                return response
            })
        }
    })
))
