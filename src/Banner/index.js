import React, {Component} from 'react'
import classnames from 'classnames'
import marked from 'marked'
import {fetchBanner} from 'clients/contentful'

import 'Banner/styles.less'

class Banner extends Component {
    constructor(props) {
        super(props)
        this.state = {
            singleBanner: []
        }
    }

    componentDidMount() {
        fetchBanner(this.props.source).then(response => this.setState({singleBanner: response}))
    }

    buildSingleBanner = () => this.state.singleBanner.map(banner => {
        const style = { backgroundImage: `url('${banner.image.background}?fm=jpg&q=30')` }
        const title = (banner.title) ? (<h3>{banner.title}</h3>) : null
        const titleImage = (banner.image.title) ? (<img src={`${banner.image.title.url}?fm=jpg&q=30`} alt={banner.image.title.alt} />) : null
        const paragraph = (banner.paragraph) ? (<p dangerouslySetInnerHTML={{__html: marked(banner.paragraph)}} />) : null
        const paragraphImage = (banner.image.paragraph) ? (<img src={`${banner.image.paragraph}?fm=jpg&q=30`} />) : null
        const link = (banner.link) ? (<a className='btn btn-secondary' href={banner.link.url} target={banner.link.target}>{banner.link.text}</a>) : null
        const bannerClass = classnames('single-banner', {'align-left': banner.alignment === "left", 'align-right': banner.alignment === "right"})
        return (<article key={banner.id} className={bannerClass} style={style}>
            <div className='info-container'>
                <div className='information'>
                    {title}
                    {titleImage}
                    {paragraph}
                    {paragraphImage}
                    {link}
                </div>
            </div>
        </article>)
    })

    render() {
        return(
            <section className='banner'>
                {this.buildSingleBanner()}
            </section>
        )
    }
}

export default Banner
