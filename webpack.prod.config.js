const webpackBase = require('./webpack.config.js')
const merge = require('webpack-merge')
const CopyWebpackPlugin = require('copy-webpack-plugin')

module.exports = merge(webpackBase,
    {
        mode: 'production'
    }
)
