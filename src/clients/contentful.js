import axios from 'axios'
import {getContentfulEnvironment, getContentfulCredentials, isMobile} from 'libs/config'

const URL = `https://preview.contentful.com/spaces/${getContentfulEnvironment()}/environments/master/entries`

const HEADERS = {
    headers: {
        Accept: 'application/json',
        'X-Contentful-User-Agent': 'sdk contentful.js/0.0.0-determined-by-semantic-release',
        Authorization: getContentfulCredentials()
    }
}

const optionalDataMenu = (item) => (item.fields.url) ? {url: item.fields.url, target: item.fields.target} : {} 
const parseMenuData = ({data: {items}}) => items.map((item) => ({title: item.fields.title, id: item.sys.id, ...optionalDataMenu(item)}))

export const fetchMenu = () => {
    const params = {
        content_type: 'menuPrincipal',
        order: 'fields.order'
    }
    return axios.get(URL, {params, ...HEADERS}).then((response) => Promise.resolve(parseMenuData(response)))
}

const getImageUrl = (includes) => id => includes.Asset.find(image => image.sys.id === id)

const getMainPromotionImage = (item, includes) => {
    const idImage = (isMobile()) ? item.fields.imageMobile.sys.id : item.fields.image.sys.id
    const image = getImageUrl(includes)(idImage)
    return (image.fields.file) ? {imageUrl: image.fields.file.url} : {}
}
const parseMainPromotionData = ({data: {includes, items}}) => items.map(item => ({title: item.fields.title, linkText: item.fields.actionTitle, url: item.fields.actionUrl, target: item.fields.targetActionUrl, ...getMainPromotionImage(item, includes)})).shift()

export const fetchMainPromotion = () => {
    const params = {
        content_type: 'homeHeader'
    }
    return axios.get(URL, {params, ...HEADERS}).then((response) => Promise.resolve(parseMainPromotionData(response)))
}

const filterItems = (items) => (filter) => items.filter(({fields}) => fields.seccion === filter).map(({fields}) => ({title: fields.texto, url: fields.url, target: fields.target}))
const parseFooterLinks = ({data: {items}}) => ({
    information: filterItems(items)('nuestro-banco'),
    helpDesk: filterItems(items)('servicio-al-cliente')
})
export const fetchFooterLinks = () => {
    const params = {
        content_type: 'footerLinks',
        order: 'fields.order'
    }
    return axios.get(URL, {params, ...HEADERS}).then(response => Promise.resolve(parseFooterLinks(response)))
}

const getBannerImage = (includes, item) => {
    const background = (item.fields.image) ? {background: getImageUrl(includes)(item.fields.image.sys.id).fields.file.url} : {}
    const title = (item.fields.imagenTitulo) ? {
        title: {
            url: getImageUrl(includes)(item.fields.imagenTitulo.sys.id).fields.file.url,
            alt: getImageUrl(includes)(item.fields.imagenTitulo.sys.id).fields.file.title
        }
    } : {}
    const paragraph = (/\(\/\/image/.test(item.fields.infoText)) ? {paragraph: /\((\/\/.+)\)/.exec(item.fields.infoText)[1]} : {}
    return {...background, ...title, ...paragraph}
}
const fixRelativeUrl = url => (/^(http|\/)/.test(url)) ? url : ['/', url].join('') 
const buildLink = ({fields}) => (fields.actionTitle) ? {link: {text: fields.actionTitle, url: fixRelativeUrl(fields.actionUrl), target: fields.targetActionUrl}} : {}
const parseBanner = ({data: {includes, items}}) => items.filter(item => item.fields.contentAlignment !== undefined).map(item => {
    const paragraph = (/\(\/\/image/.test(item.fields.infoText)) ? {} : {paragraph: item.fields.infoText}
    return {
        id: item.sys.id,
        title: item.fields.title,
        ...paragraph,
        ...buildLink(item),
        alignment: item.fields.contentAlignment,
        image: getBannerImage(includes, item) 
    }
})
export const fetchBanner = (source) => {
    const params = {
        content_type: source,
        order: 'fields.order'
    }
    return axios.get(URL, {params, ...HEADERS}).then(response => Promise.resolve(parseBanner(response)))
}
