# Sitio público

Este proyecto es una prueba de concepto del site en react, pero conteniendo todos los paises en un solo repositorio, aprovechando el poder de react de componetizar los elementos.

### MVPS's
1. Crear el home page para comparar su performance contra el proyecto creado en angular.

### Tecnologías

Listado de las principales librerias usadas.

* [ES6]: Estandar javascript principalmente usado.
* [React]: Creación de componentes
* [Axios]: Llamado a servicios
* [Webpack]: Generador de bundles y configuración del proyecto
* [Classnames]: Asignación de clases css más dinámicas a los tags html

### Instalación

Debes instalar la herramienta NVM, con esto puedes manejar las versiones de node y npm de este proyecto independiente de cualquier otro que tengas, puedes usar el comando siguiente, o revisar la [página de nvm](https://github.com/nvm-sh/nvm).
```
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
```
Luego ejecutar:
```
nvm use
npm i
```
Con esto ya habrás instalado todo lo necesario.

### Desarrollo

Para desarrollar solo existe el comando `npm start`, el cual ejecuta el plugin webpack-dev-server que levanta el sitio localmente en localhost:8000. Los cambios que realices en código se verán reflejados automáticamente.
La configuración del servidor están en el archivo de configuración `webpack.config.js`
