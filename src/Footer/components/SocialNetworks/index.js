import React from 'react'
import PhoneSVG from 'icons/phone.svg?in'
import FacebookSVG from 'icons/facebook.svg?in'
import TwitterSVG from 'icons/twitter.svg?in'
import InstagramSVG from 'icons/instagram.svg?in'
import YoutubeSVG from 'icons/youtube.svg?in'

import 'Footer/components/SocialNetworks/styles.less'

const SocialNetworks = () => (
    <section className='social-networks'>
        <a className='btn btn-white' href='/ayuda-y-contacto'>
            <i dangerouslySetInnerHTML={{__html: PhoneSVG}}></i>Robo o Extrav&iacute;o</a>
        <ul>
            <li><a href='https://www.facebook.com/bancofalabella' target='_blank' title='Facebook'>
                <i dangerouslySetInnerHTML={{__html: FacebookSVG}}></i>
            </a></li>
            <li><a href='https://twitter.com/Banco_Falabella' target='_blank' title='Twitter'>
                <i dangerouslySetInnerHTML={{__html: TwitterSVG}}></i>
            </a></li>
            <li><a href='https://www.instagram.com/banco_falabella/' target='_blank' title='Instagram'>
                <i dangerouslySetInnerHTML={{__html: InstagramSVG}}></i>
            </a></li>
            <li><a href='https://www.youtube.com/user/BancoFalabellaChile"' target='_blank' title='Youtube'>
                <i dangerouslySetInnerHTML={{__html: YoutubeSVG}}></i>
            </a></li>
        </ul>
    </section>
)

export default SocialNetworks
