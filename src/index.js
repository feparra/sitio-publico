import React, {Component} from 'react'
import {hydrate, render} from 'react-dom'
import App from './App'

if('serviceWorker' in navigator) {
    console.log('enabled service worker')
    navigator.serviceWorker.register('/imageWorker.js', {scope: './'}).then(reg => {
        if(reg.installing) console.log("service worker installing")
        if(reg.waiting) console.log("service worker installed")
        if(reg.active) console.log("service worker active")
    }).catch(error => console.log("Registration failed:", error))
}

const appElement = document.getElementById('app')
if(appElement.hasChildNodes()) {
    hydrate(<App />, appElement)
}
else {
    render(<App />, document.getElementById('app'))
}
